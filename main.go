package main

import (
	"depslive/redis"
	"depslive/webserver"
	"fmt"
	"os"
)

const defaultPort = "8080"

var stationService redis.StationService

func main() {
	stationService.Load()
	fmt.Println("starting server")

	router := webserver.RegisterRoutes()
	port := os.Getenv("PORT")

	if len(port) == 0 {
		port = defaultPort
	}

	router.Run(":" + port)
}
