package mailer

import (
	"bytes"
	"fmt"
	"html/template"

	"github.com/go-gomail/gomail"
)

var templates *template.Template

func init() {
	templates = template.Must(template.ParseGlob("mailer/templates/*"))
}

var (
	activateLinkFormat = "http://localhost:8080/activate/%s?token=%s"
	removeLinkFormat   = "http://localhost:8080/remove/%s?token=%s"
	pageURLFormat      = "http://localhost:8080/%s"
)

type ActivationEmail struct {
	ActivateLink string
	RemoveLink   string
	PageURL      string
}

func SendActivationEmail(email, url, activateToken, removeToken string) {
	var tpl bytes.Buffer
	activationEmail := ActivationEmail{
		ActivateLink: fmt.Sprintf(activateLinkFormat, url, activateToken),
		RemoveLink:   fmt.Sprintf(removeLinkFormat, url, removeToken),
		PageURL:      fmt.Sprintf(pageURLFormat, url),
	}

	templates.ExecuteTemplate(&tpl, "activationEmail", activationEmail)

	m := gomail.NewMessage()
	m.SetHeader("From", "ivan.yunda@gmail.com")
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Your personal trains board")
	m.SetBody("text/html", tpl.String())

	d := gomail.NewDialer("smtp.gmail.com", 465, "ivan.yunda@gmail.com", "Jesuslove665665665")

	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}
}
