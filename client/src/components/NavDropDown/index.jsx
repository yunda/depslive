import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';

import styles from './styles.css';

export const NavItem = ({url, children, onClick}) => (
  <Link onClick={onClick} className={styles.navItem} to={url}>{children}</Link>
);

export const NavToggle = ({className, children, onClick}) => (
  <button
    className={`btn ${styles.toggle} ${className}`}
    onClick={onClick}
    type="button"
    aria-haspopup="true"
    aria-expanded="false">
    {children}
  </button>
);

export class NavDropDown extends PureComponent {

  static propTypes = {
    closeOnClickOutside: PropTypes.bool,
    className: PropTypes.string,
  }

  static defaultProps = {
    closeOnClickOutside: true,
    className: '',
  }

  state = {
    open: false,
  }

  wrapperEl = null

  componentDidMount() {
    if (this.props.closeOnClickOutside) {
      document.addEventListener('mousedown', this.onDocumentMousedown);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.onDocumentMousedown);
  }

  onDocumentMousedown = e => {
    if (this.wrapperEl && !this.wrapperEl.contains(e.target)) {
      this.onClickOutside();
    }
  }

  onClickOutside() {
    this.close();
  }

  close = () => {
    this.setState({ open: false });
  }

  onToggleClick = () => {
    this.setState({ open: !this.state.open });
  }

  renderChildren() {
    const dropDownItems = [];
    let toggle
    
    React.Children.forEach(this.props.children, child => {
      if (child.type === NavToggle) {
        toggle = React.cloneElement(child, { onClick: this.onToggleClick });
        return;
      }

      if (child.type === NavItem) {
        dropDownItems.push(React.cloneElement(child, { onClick: this.close }));
        return;
      }
      
      dropDownItems.push(child);
    });

    return (
      <Fragment>
        {toggle}
        <div className={`${styles.list} ${!this.state.open ? 'hidden' : ''}`}>
          {dropDownItems}
        </div>
      </Fragment>
    );
  }

  render() {
    const { className } = this.props;

    return (
      <div className={`${styles.navDropDown} ${className} ${this.state.open ? styles.open : ''}`} ref={el => this.wrapperEl = el}>
        {this.renderChildren()}        
      </div>
    );
  }
}
