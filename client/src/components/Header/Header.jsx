import React from 'react';
import { Link } from 'react-router-dom';

import Logo from './logo.svgx';
import Navigation from '../../containers/Navigation/Navigation';

import styles from './styles.css';

const Header = ({hideNav, slogan, homePage}) => (
  <header className={styles.header}>
    <section className={styles.logo}>
      {homePage ? <Logo /> : (
        <Link className={styles.homeLink} to="/">
          <Logo />
        </Link>
      )}
    </section>
    <section className={styles.navigation}>
      {!hideNav && <Navigation />}
      {slogan && <p className={styles.slogan}>{slogan}</p>}
    </section>
  </header>
);

export default Header;
