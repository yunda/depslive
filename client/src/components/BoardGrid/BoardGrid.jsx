import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import uniqueId from 'lodash/uniqueId';
import dateFormat from 'dateformat';
import styles from './styles.css';

export default class BoardGrid extends PureComponent {
  static propTypes = {
    services: PropTypes.arrayOf(PropTypes.object),
    noDestination: PropTypes.bool,
  }

  static defaultProps = {
    services: [],
    noDest: false,
  }
  
  timeFormatter(date) {
    return dateFormat(date, 'HH:MM');
  }

  renderDepartureTime(std, etd) {
    const stdDate = new Date(std);
    const etdDate = etd && new Date(etd);

    if (!etdDate || stdDate.getTime() === etdDate.getTime()) {
      return <span>{this.timeFormatter(stdDate)}</span>;
    }

    return (
      <Fragment>
        <del className={styles.timeOld}>{this.timeFormatter(stdDate)}</del>
        <ins className={styles.timeNew}>{this.timeFormatter(etdDate)}</ins>
      </Fragment>
    );
  }

  renderStatus(status) {
    if (status === 'Delayed') {
      return <span className={styles.statusDelayed}>{status}</span>;
    }

    if (status === 'Canceled') {
      return <span className={styles.statusCanceled}>{status}</span>;
    }

    return <span className={styles.statusOnTime}>{status}</span>;
  }

  renderRows() {
    return this.props.services.map(service => (
      <tr className={styles.tableRow} key={uniqueId('service-row-')}>
        <td>
          <div className={styles.timeCell}>
            {this.renderDepartureTime(service.std, service.etd)}
          </div>
        </td>
        <td>
          <div className={styles.destinationCell}>
            {service.destinationName}
          </div>
        </td>
        <td>
          <div className={styles.statusCell}>
            {this.renderStatus(service.status)}
          </div>
        </td>
        <td>
          <div className={styles.platformCell}>
            {service.platform || '–'}
          </div>
        </td>
        {!this.props.noDestination && (
          <Fragment>
            <td>
              <div className={styles.stopsCell}>
                {service.stopsNumber}
              </div>
            </td>
            <td>
              <div className={styles.travelTimeCell}>
                {service.travelTime}
              </div>
            </td>
            <td>
              <div className={styles.arrivalCell}>
                {this.timeFormatter(new Date(service.arrival))}
              </div>
            </td>
          </Fragment>
        )}
      </tr>
    ));
  }

  render() {
    if (this.props.services.length === 0) {
      return <p>No data</p>;
    }

    return (
      <table className={styles.table}>
        <thead>
          <tr>
            <th className={styles.dueTh}>Due</th>
            <th className={styles.destTh}>Destination</th>
            <th>Status</th>
            <th>Platform</th>
            {!this.props.noDestination && (
              <Fragment>
                <th>Stops</th>
                <th>Travel Time</th>
                <th>Arrival</th>
              </Fragment>
            )}
          </tr>
        </thead>
        <tbody>
          {this.renderRows()}
        </tbody>
      </table>
    );
  }
}