import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './styles.css';

const BoardHeader = ({from, to}) => (
  <div className={styles.boardHeader}>
    <h1 className={styles.codes}>
      <span title={from.name}>{from.crsCode}</span>
      <span>&nbsp;→&nbsp;</span>
      <span title={to.name}>{to.crsCode}</span>
    </h1>
    <p className={styles.names}>
      <Link to={`/page/station/${from.url}`}>{from.name}</Link>
      <span className={styles.pointer} />
      <Link to={`/page/station/${to.url}`}>{to.name}</Link>
    </p>
  </div>
);

BoardHeader.propTypes = {
  from: PropTypes.shape({
    crsCode: PropTypes.string,
    name: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
  to: PropTypes.shape({
    crsCode: PropTypes.string,
    name: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
};

export default BoardHeader;
