import React from 'react';

import styles from './styles.css';

const NotActivated = ({show}) => (
    show && 
    <div className={styles.notActivated}>
        <p>This page has not been activated yet. Please check your mailbox for activation instructions. Hurry, the page may be expiring soon.</p>
    </div>
);

export default NotActivated;
