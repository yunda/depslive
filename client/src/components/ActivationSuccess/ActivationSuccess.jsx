import React from 'react';

const ActivationSuccess = ({match}) => (
    <div>
        <h1>Your page has been activated 🎉</h1>
        <p><a href={`/${match.params.url}`}>Take me there now</a></p>
    </div>
);

export default ActivationSuccess;