import React from 'react';

const RemovalSuccess = ({match}) => (
    <div>
        <h1>Boom! 💣</h1>
        <p>Page <b>/{match.params.url}</b> has been removed forever.</p>
    </div>
);

export default RemovalSuccess;
