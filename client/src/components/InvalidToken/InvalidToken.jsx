import React from 'react';
import { Link } from "react-router-dom";

const InvalidToken = () => (
    <div>
        <h1>Invalid token 🤨</h1>
        <p>It is possible that your activation token has expired.</p>
        <p>Try clicking the link from your email again. If that doesn't help <Link to="/">start new serach</Link>.</p>
    </div>
);

export default InvalidToken;
