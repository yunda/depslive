
import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Icon extends Component {
    static propTypes = {
      name: PropTypes.string.isRequired,
      className: PropTypes.string,
    }

    static defaultProps = {
      className: '',
    }

    render() {
      const { name, className } = this.props;
      const IconComponent = require(`./icons/${name}.svgx`);

      if (!IconComponent) {
        return null;
      }

      return <IconComponent className={className} />;
    }
};
