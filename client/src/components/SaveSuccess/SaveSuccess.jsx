import React from 'react';

const SaveSuccess = ({match}) => (
  <div>
    <h2>Save Page</h2>
    <p>Activation email has been sent to you. Please check your mailbox and follow the instructions within 2 hours to finish the process.</p>
  </div>
);

export default SaveSuccess;
