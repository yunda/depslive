import React from 'react';
import { Link } from 'react-router-dom';

import nreLogoBlue from './nre-logo-blue.png';
import nreLogoWhite from './nre-logo-white.png';
import styles from './styles.css';

const Footer = ({dark}) => (
  <footer className={styles.footer}>
    <section className={styles.poweredBy}>
      <a className={styles.poweredByLink} href="http://www.nationalrail.co.uk" target="_blank">
        <img className={styles.logo} alt="NRE logo" src={dark ? nreLogoWhite : nreLogoBlue} />
      </a>
    </section>
    <section className={styles.creatorCopy}>
      <p>Created and maintained by <a href="https://github.com/yunda">Ivan Yunda</a>
      <br />To report a problem or suggest an improvement write to: <a href="mailto:hi@departures.live">hi@departures.live</a></p>
    </section>
    <section className={styles.links}>
      <Link to="/page/popular-routes">Popular routes</Link>
      <Link to="/page/all-stations">All stations</Link>
    </section>
  </footer>
);

export default Footer;
