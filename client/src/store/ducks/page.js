import keyMirror from 'key-mirror';
import { createAction, handleActions } from 'redux-actions';

// Actions
export const routesActions = keyMirror({
    SET_FROM: null,
    SET_TO: null,
    REVERSE: null,
    ADD_ROUTE: null,
})

// Action creators
export const setFrom = createAction(routesActions.SET_FROM);
export const setTo = createAction(routesActions.SET_TO);
export const reverse = createAction(routesActions.REVERSE);
export const addRoute = createAction(routesActions.ADD_ROUTE);

// Reducer
const defaultState = {
  newRoute: {
    from: {},
    to: {},
  },
  activeRoute: 0,
  routes: [],
  activated: true,
};

export default handleActions({
  [setFrom](state, action) {
    return {
      ...state,
      newRoute: {
        ...state.newRoute,
        from: action.payload,
      },
    };
  },
  [setTo](state, action) {
    return {
      ...state,
      newRoute: {
        ...state.newRoute,
        to: action.payload,
      },
    };
  },
  [reverse](state, action) {
    return {
      ...state,
      newRoute: {
        from: state.newRoute.to,
        to: state.newRoute.from,
      },
    };
  },
  [addRoute](state, action) {
    return {
      ...state,
      newRoute: defaultState.newRoute,
      routes: [...state.routes, action.payload],
    };
  },
}, Object.assign({}, defaultState, window.__PRELOADED_STATE__ || {}));
