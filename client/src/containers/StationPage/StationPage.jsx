import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { gql } from 'apollo-boost';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { Query } from 'react-apollo';
import get from 'lodash/get';
import { setFrom } from '../../store/ducks/page';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Icon from '../../components/Icon/Icon';
import StationBoard from '../StationBoard/StationBoard';

import styles from './styles.css';

const STATION_QUERY = gql`
  query RootQuery($url: String!) {  
    station(url: $url) {
      name
      crsCode
      url
      atmMachine
      toilets
      wiFi
      telephones
      telephoneType
      waitingRoom
      stationBuffet
      ticketMachine
      stepFreeAccess
      stepFreeAccessNote
      cycleStorage
      longitude
      latitude
    	address{
        lines
        postCode
      }
    	ticketOffice{
        available
        annotation
        dayAndTimeAvailability{
          dayType
          openingHours{
            startTime
            endTime
            unavailable
            twentyFourHours
          }
          holidayTypes{
            allBankHoldays
          }
        }
      }
    }
  }
`;

const mapDispatchToProps = {
  setFrom,
};

@withRouter
@connect(null, mapDispatchToProps)
export default class StationPage extends PureComponent {

  onFilterByDestClick = ({ name, crsCode, url }) => () => {
    this.props.setFrom({ name, crsCode, url });
    this.props.history.push('/');
  }
  
  getVariables() {
    return { url: get(this.props, 'match.params.url') };
  }

  renderFacilityIcon(value) {
    return <Icon name={value ? 'tick' : 'cross'} className={value ? styles.iconGreen : styles.iconRed} />;
  }

  renderFacilities(station) {
    return (
      <div className={styles.facilities}>
        <div className={styles.facilitiesCol}>
          <p>{this.renderFacilityIcon(station.atmMachine)}Atm Machine</p>
          <p>{this.renderFacilityIcon(station.toilets)}Toilet</p>
          <p>{this.renderFacilityIcon(station.wiFi)}Wi-fi</p>
        </div>
        <div className={styles.facilitiesCol}>
          <p>{this.renderFacilityIcon(station.ticketMachine)}Ticket Machine</p>
          <p>{this.renderFacilityIcon(station.telephones)}Telephones</p>
          <p>{this.renderFacilityIcon(station.waitingRoom)}Waiting Room</p>
        </div>
        <div className={styles.facilitiesCol}>
          <p>{this.renderFacilityIcon(station.stepFreeAccess)}Step Free Access</p>
          <p>{this.renderFacilityIcon(station.cycleStorage)}Cycle Storage</p>
          <p>{this.renderFacilityIcon(station.stationBuffet)}Station Buffet</p>
        </div>
      </div>
    )
  }

  getOpeningHours({ startTime, endTime, unavailable, twentyFourHours }) {
    if (unavailable) {
      return 'Closed';
    }

    if (twentyFourHours) {
      return '24h'
    }

    return `${startTime.substring(0,5)} – ${endTime.substring(0,5)}`;
  }

  renderTicketOffice(ticketOffice) {
    return (
      <div className={styles.ticketOffice}>
        <h2>Ticket Office</h2>
        {ticketOffice.dayAndTimeAvailability.map(item => (
          <dl className={styles.hoursRow}>
            <dt className={styles.dayType}>{item.dayType}</dt>
            <dd className={styles.hours}>{this.getOpeningHours(item.openingHours)}</dd>
          </dl>
        ))}
      </div>
    );
  }

  renderAddress(address, longitude, latitude) {
    return (
      <div className={styles.address}>
        <h2>Address</h2>

      </div>
    );
  }

  render() {
    return (
      <Query query={STATION_QUERY} variables={this.getVariables()}>
        {({ loading, error, data }) => {

          if (loading) return <div>Loading...</div>;
          if (error) return <div>Error :(</div>;

          const { station } = data;

          console.log(station);
          
          return (
            <Fragment>
              <Header hideNav slogan="The quickest way to check UK live train departures" />
              <section className="main">
                <h1 className={styles.heading}>{station.name}</h1>
                <div className="row">
                  <section className={styles.content}>
                    {this.renderFacilities(station)}
                    <h2>Departures</h2>
                    <StationBoard crsCode={station.crsCode} />
                    <button
                      className={styles.filterButton}
                      onClick={this.onFilterByDestClick(station)}>
                      Filter by destination
                    </button>
                  </section>
                  <aside className={styles.sideBar}>
                    {this.renderTicketOffice(station.ticketOffice)}
                  </aside>
                </div>
              </section>
              <Footer />
            </Fragment>
          );
        }}
      </Query>
    );
  }
}
