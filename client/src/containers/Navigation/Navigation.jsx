import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import uniqueId from 'lodash/uniqueId';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

import AddRoute from '../AddRoute/AddRoute';
import SaveModal from '../SaveModal/SaveModal';
import { NavDropDown, NavToggle, NavItem } from '../../components/NavDropDown';
import Icon from '../../components/Icon/Icon';

import styles from './styles.css';

const mapStateToProps = state => ({
  routes: state.page.routes
});

@connect(mapStateToProps)
export default class Navigation extends PureComponent {
  state = {
    isAddRouteOpen: false,
    isSaveModalOpen: false,
  }

  onAddClick = () => {
    this.setState({ isAddRouteOpen: true });
  }

  onAddClose = () => {
    this.setState({ isAddRouteOpen: false });
  }

  onSaveClick = () => {
    this.setState({ isSaveModalOpen: true });
  }

  onSaveClose = () => {
    this.setState({ isSaveModalOpen: false });
  }

  newCount() {
    const oldBoardsCount = get(window, '__PRELOADED_STATE__.routes.length', 0);
    const currentBoardsCount = this.props.routes.length;
    
    return currentBoardsCount - oldBoardsCount;
  }

  render() {
    return (
      <nav className={styles.nav}>
        {!isEmpty(this.props.routes) && (
          <NavDropDown>
            <NavToggle className={styles.navBtn}>
              <Icon className={styles.iconBurger} name="burger" />
              <span className={styles.btnText}>My Routes</span>
              {!!this.newCount() && <span className={styles.counter}>{this.newCount()}</span>}
            </NavToggle>
            {this.props.routes.map(route => (
              <NavItem key={uniqueId('nav-item-')} url="/">{route.from.name}&nbsp;&rarr;&nbsp;{route.to.name}</NavItem>
            ))}
          </NavDropDown>
        )}
        {!!this.newCount() && (
          <button className={styles.navBtn} onClick={this.onSaveClick}>
            <Icon className={styles.iconSave} name="save" />
            <span className={styles.btnText}>Save</span>
          </button>
        )}
        <button className={styles.navBtn} onClick={this.onAddClick}>
          <Icon className={styles.iconSearch} name="search" />
          <span className={styles.btnText}>Search Route</span>
        </button>
        <AddRoute isOpen={this.state.isAddRouteOpen} onRequestClose={this.onAddClose} />
        <SaveModal isOpen={this.state.isSaveModalOpen} onRequestClose={this.onSaveClose} />
      </nav>
    );
  }
};
