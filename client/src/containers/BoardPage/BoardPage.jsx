import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import NotActivated from '../../components/NotActivated/NotActivated';
import Board from '../Board/Board';

import styles from './styles.css';

const mapStateToProps = state => ({
  activated: state.page.activated,
});

@connect(mapStateToProps)
export default class BoardPage extends PureComponent {
  static propTypes = {
    activated: PropTypes.bool,
  }

  componentDidMount() {
    document.body.classList.add(styles.body);
  }

  componentWillUnmount() {
    document.body.classList.remove(styles.body);
  }

  render() {
    return (
      <Fragment>
        <NotActivated show={!this.props.activated} />
        <Header />
        <section className="main">
          <Board />
        </section>
        <Footer dark />
      </Fragment>
    );
  }
}
