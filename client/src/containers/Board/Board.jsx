import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';

import BoardGrid from '../../components/BoardGrid/BoardGrid';
import BoardHeader from '../../components/BoardHeader/BoardHeader';

import styles from './styles.css';

const GET_BOARD = gql`
  query RootQuery($from: String!, $to: String!) {  
    board(from: $from, to: $to) {
      services{
        std
        etd
        destinationName
        platform
        stopsNumber
        travelTime
        arrival
        status
      }
    }
  }
`;

const mapStateToProps = state => ({
  routes: state.page.routes,
});

@connect(mapStateToProps)
export default class Board extends PureComponent {
  static propTypes = {
    routes: PropTypes.arrayOf(PropTypes.shape({
      from: PropTypes.object,
      to: PropTypes.object,
    }))
  }

  getVariables(route) {
    return { from: route.from.crsCode, to: route.to.crsCode };
  }

  render() {
    const firstRoute = this.props.routes[0];
    
    if (!firstRoute) {
      return null;
    }

    const vars = this.getVariables(firstRoute);

    return (
      <div className={styles.board}>
        <BoardHeader {...firstRoute} />
        <Query query={GET_BOARD} variables={vars} notifyOnNetworkStatusChange>
          {({ loading, error, data, refetch, networkStatus }) => {
            const refetching = networkStatus === 4;
            
            if (loading && !refetching) return <div>Loading...</div>;
            if (error) return <div>Error :(</div>;
            
            const { services } = data.board;

            return (
              <div className={styles.board}>
                <button
                  onClick={() => {refetch()}}
                  className={styles.refreshBtn}
                  disabled={refetching}
                  title="Refresh">
                  {refetching ? 'Refreshing...' : <span className={styles.refreshIcon}>↻</span>}
                </button>
                <BoardGrid services={services} />
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}
