import React, { PureComponent } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import BoardPage from '../BoardPage/BoardPage';
import HomePage from '../HomePage/HomePage';
import AllStationsPage from '../AllStationsPage/AllStationsPage';
import StationPage from '../StationPage/StationPage';

import ActivationSuccess from '../../components/ActivationSuccess/ActivationSuccess';
import InvalidToken from '../../components/InvalidToken/InvalidToken';
import RemovalSuccess from '../../components/RemovalSuccess/RemovalSuccess';

import styles from './styles.css';

export default class App extends PureComponent {
  render() {
    return (
      <Router>
        <main className={styles.app}>
          <Switch>
            <Route exact path="/page/all-stations" component={AllStationsPage} />
            <Route exact path="/page/station/:url" component={StationPage} />
            <Route exact path="/activation-success/:url" component={ActivationSuccess} />
            <Route exact path="/invalid-token" component={InvalidToken} />
            <Route exact path="/removal-success/:url" component={RemovalSuccess} />
            <Route exact path="/:url" component={BoardPage} />
            <Route exact path="/trains/from/:from/to/:to" component={BoardPage} />
            <Route path="/" component={HomePage} />
          </Switch>
        </main>
      </Router>
    );
  }
}
