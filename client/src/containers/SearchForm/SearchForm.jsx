import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { addRoute, setFrom, setTo, reverse } from '../../store/ducks/page';
import StationInput from '../StationInput/StationInput';

import styles from './styles.css';

const mapStateToProps = state => ({
  from: state.page.newRoute.from,
  to: state.page.newRoute.to,
});

const mapDispatchToProps = {
  addRoute,
  setFrom,
  setTo,
  reverse,
};

@connect(mapStateToProps, mapDispatchToProps)
export default class SearchForm extends Component {
  static propTypes = {
    className: PropTypes.string,
    dark: PropTypes.bool,
    onSubmit: PropTypes.func,
    hideSubmit: PropTypes.bool,
  }

  static defaultProps = {
    className: '',
    dark: false,
    onSubmit: () => {},
    hideSubmit: false,
  }

  state = {
    from: {
      value: '',
      open: false,
    },
    to: {
      value: '',
      open: false,
    },
  }

  onSelect = name => item => {
    if (name === 'from') {
      this.props.setFrom(item);
    } else if (name === 'to') {
      this.props.setTo(item);
    }
  }

  onChange = name => value => {
    this.setState({
      [name]: {
        value,
        open: value.length > 1,
      },
    });
  }

  submit = e => {    
    e.preventDefault();
    this.props.onSubmit();
  }

  isSubmitDisabled() {
    return isEmpty(this.props.from) || isEmpty(this.props.to);
  }

  reverse = () => {
    this.setState({
      from: { ...this.state.to, open: false },
      to: { ...this.state.from, open: false },
    });
    this.props.reverse();
  }

  render() {
    const { className, hideSubmit, dark, from, to } = this.props;

    return (
      <form
        onSubmit={this.submit}
        className={`${styles.form} ${dark ? styles.dark : ''} ${className}`}>
        <StationInput
          ref={elem => this.firstInput = elem}
          name="from"
          placeholder="From" 
          onSelect={this.onSelect('from')}
          onChange={this.onChange('from')}
          value={this.state.from.value}
          open={this.state.from.open}
          selected={from}
          dark={dark}
          tabIndex={1}
          focus />
        <button type="button" onClick={this.reverse} className={styles.arrowBtn} tabIndex={4}>
          <span className={styles.icon}>→</span>
        </button>
        <StationInput
          name="to"
          placeholder="To"
          onSelect={this.onSelect('to')}
          onChange={this.onChange('to')}
          value={this.state.to.value}
          open={this.state.to.open}
          className={styles.input}
          selected={to}
          dark={dark}
          tabIndex={2} />
        {!hideSubmit && (
          <button
            type="submit"
            disabled={this.isSubmitDisabled()}
            className={styles.submit}
            tabIndex={3}>Go</button>
        )}
      </form>
    );
  }
}
