import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import includes from 'lodash/includes';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import sortBy from 'lodash/sortBy';
import groupBy from 'lodash/groupBy';
import uniqueId from 'lodash/uniqueId';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

import styles from './styles.css';

const STATIONS_QUERY = gql`
  query RootQuery {  
    stations {
      name
      crsCode
      url
    }
  }
`;

export default class AllStationsPage extends PureComponent {

  state = {
    filterText: ''
  }

  updateFilter = debounce(value => {
    console.log(value);
    this.setState({ filterText: value.toLowerCase() });
  }, 100)

  onFilterChange = (e) => {
    e.persist();
    this.updateFilter(e.target.value);
  }

  getGroupedStations(stations) {
    const filtered = stations.filter(station => includes(station.name.toLowerCase(), this.state.filterText));
    const grouped = groupBy(filtered, station => station.name[0].toUpperCase());

    return sortBy(Object.keys(grouped)).map(letter => ({
      letter,
      stations: sortBy(grouped[letter], station => station.name.toLowerCase()),
    }));
  }

  renderIndex(groupedStations) {
    return groupedStations.map(item => (
      <section className={styles.indexSection} key={uniqueId('index-')}>
        <div className={styles.letter}>
          <h1>{item.letter}</h1>
        </div>
        <div className={styles.contents}>
          <ul className={styles.stationsList}>
            {item.stations.map(station => (
              <li className={styles.stationItem} key={uniqueId('station-')}>
                <a href={`/page/station/${station.url}`}>{station.name}</a>
              </li>
            ))}
          </ul>
        </div>
      </section>
    ));
  }

  render() {
    return (
      <Query query={STATIONS_QUERY}>
        {({ loading, error, data }) => {

          if (loading) return <div>Loading...</div>;
          if (error) return <div>Error :(</div>;
          
          const groupedStations = this.getGroupedStations(data.stations);
          
          return (
            <Fragment>
              <Header hideNav slogan="The quickest way to check UK live train departures" />
              <section className="main">
                <h1>All UK rail stations</h1>
                <input
                  className="input"
                  placeholder="Search"
                  onChange={this.onFilterChange}
                />
                {this.renderIndex(groupedStations)}
              </section>
              <Footer />
            </Fragment>
          );
        }}
      </Query>
    );
  }
}
