import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import isArray from 'lodash/isArray';
import uniqueId from 'lodash/uniqueId';
import cn from 'classnames';

import styles from './styles.css';

const SAVE_PAGE = gql`
  mutation RootMutation($url: String!, $email: String!, $routes: [RouteInput]!, $override: Boolean = false) {
    createPage(url: $url, email: $email, routes: $routes, override: $override) {
      url
      email
      routes{
        from{
          crsCode
          name
        }
        to{
          crsCode
          name
        }
      }
    }
  }
`;

const mapStateToProps = state => ({
  routes: state.page.routes
});

@connect(mapStateToProps)
export default class SaveModal extends PureComponent {
  static propsTypes = {
    onRequestClose: PropTypes.func,
    isOpen: PropTypes.bool,
    routes: PropTypes.arrayOf(PropTypes.object),
  }
  
  static defaultProps = {
    onRequestClose: () => {},
    isOpen: false,
    routes: [],
  }

  state = {
    url: '',
    email: '',
    override: false
  }

  onChange = name => ({target}) => {
    const value = target.type === 'checkbox' ? target.checked : target.value;
  
    this.setState({
      [name]: value,
    });
  }

  submit = savePage => e => {
    e.preventDefault();
    const variables = {
      ...this.state,
      routes: this.props.routes,
    };

    savePage({
      variables,
    });
  }

  parseErrors(error) {
    if (!error) {
      return null;
    }
  
    return error.graphQLErrors.map(err => {
      const [key, message] = err.message.split(':');

      return { key, message };
    });
  }

  renderErrors(key, errors) {
    if (!isArray(errors)) {
      return null;
    }

    const errorsByKey = errors.filter(err => err.key === key);

    return (
      <Fragment>
        {errorsByKey.map(err => <p className="form-error" key={uniqueId('error-')}>{err.message}.</p>)}
      </Fragment>
    );
  }

  hasErrors(key, errors) {
    return isArray(errors) && !!errors.find(err => err.key === key);
  }

  shouldRenderOverride(errors) {
    return isArray(errors) && !!errors.find(err => err.key === 'exists');
  }

  onError(err) {
    return null;
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        contentLabel="Example Modal"
        className={styles.modal}
        overlayClassName={styles.modalOverlay}
      >
        <button 
          className={styles.closeButton}
          role="button"
          type="button"
          onClick={this.props.onRequestClose}>Close</button>
        <h2 className={styles.title}>Save Page</h2>
        <p className={styles.subTitle}>No registration required. Unique page with your selected routes will be created and linked to your email.</p>
        <Mutation mutation={SAVE_PAGE} errorPolicy="ignore" onError={this.onError}>
          {(savePage, {error, loading, data}) => {
            const errors = this.parseErrors(error);
            
            return (
              <form onSubmit={this.submit(savePage)}>
                <div className={cn('form-group', styles.formGroup, this.hasErrors('url', errors) && 'has-errors')}>
                  <label className="label">Desired public URL</label>
                  <div className="input-group">
                    <div className="input-group-prepend">http://departures.live/</div>
                    <input className="input" type="text" value={this.state.url} onChange={this.onChange('url')} />
                  </div>
                  <p className="form-hint">URL can only contain latin letters, numbers and hyphen symbols.</p>
                  {this.renderErrors('url', errors)}
                </div>
                <div className={cn('form-group', styles.formGroup, this.hasErrors('email', errors) && 'has-errors')}>
                  <label className="label">Email</label>
                  <input className="input" type="email" value={this.state.email} onChange={this.onChange('email')} />
                  <p className="form-hint">After submitting this form you will recieve an activation link to this email.</p>
                  {this.renderErrors('email', errors)}
                </div>
                {this.shouldRenderOverride(errors) && (
                  <div className={cn('form-group', styles.existsFormGroup)}>
                    {this.renderErrors('exists', errors)}
                    <label>
                      <input
                        type="checkbox"
                        checked={this.state.override}
                        onChange={this.onChange('override')}
                        className={styles.checkbox} />
                      Overwrite my old page
                    </label>
                  </div>
                )}
                <button type="submit" className="btn btn-primary" disabled={loading}>{loading ? 'Saving...' : 'Save Page'}</button>
              </form>
            );
          }}
        </Mutation>
      </Modal>
    )
  }
}
