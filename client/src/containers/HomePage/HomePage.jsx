import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import SearchForm from '../SearchForm/SearchForm';
import { addRoute } from '../../store/ducks/page';
import { link } from 'fs';


const mapStateToProps = state => ({
  newRoute: state.page.newRoute,
});

const mapDispatchToProps = {
  addRoute,
};

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class HomePage extends PureComponent {

  onSubmit = () => {
    const { newRoute } = this.props;
    const { from, to } = newRoute;

    this.props.addRoute(newRoute);
    this.props.history.push(`/trains/from/${from.url}/to/${to.url}/`);
  }

  render() {
    return (
      <Fragment>
        <Header hideNav homePage slogan="The quickest way to check UK live train departures" />
        <section className="main">
          <SearchForm onSubmit={this.onSubmit} />
        </section>
        <Footer />
      </Fragment>
    );
  }
}
