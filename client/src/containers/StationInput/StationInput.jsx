import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Autocomplete from 'react-autocomplete';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import debounce from 'lodash/debounce';
import uniqueId from 'lodash/uniqueId';
import get from 'lodash/get';

import styles from './styles.css';

const SEARCH_STATIONS = gql`
  query RootQuery($query: String) {  
    stations(query: $query){
      name
      crsCode
      url
    }
  }
`;

@withApollo
class StationInput extends PureComponent {
  static propTypes = {
    placeholder: PropTypes.string,
    name: PropTypes.string,
    dark: PropTypes.bool,
    focus: PropTypes.bool,
    tabIndex: PropTypes.number,
    selected: PropTypes.object,
    onSelect: PropTypes.func,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string,
    open: PropTypes.bool,
  }

  static defaultProps = {
    dark: false,
    focus: false,
    value: '',
  }

  state = {
    suggestions: [],
    value: '',
    open: false,
  }

  componentWillUnmount() {
    this.setState({ value: '' });
  }

  componentWillMount() {
    this.setState({
      value: get(this.props, 'selected.name', this.props.value),
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selected !== nextProps.selected) {
      this.setState({
        value: get(nextProps, 'selected.name', '')
      });
    }

    if (this.props.value !== nextProps.value) {
      this.setState({
        value: nextProps.value,
      });
    }

    if (this.props.open !== nextProps.open) {
      this.setState({
        open: nextProps.open,
      });
    }
  }

  componentDidMount() {
    this.props.focus && this.inputElem && this.inputElem.focus();
  }

  fetchSuggestions = debounce(async (value) => {
    const { client } = this.props;
    
    try {
      const data = await client.query({
        query: SEARCH_STATIONS,
        variables: {
          query: value
        }
      });

      this.setState({ suggestions: data.data.stations || [] });
    } catch(e) {
      console.log(e);
    }
  }, 300)

  onChange = (e) => {
    const value = e.target.value;
    
    if (value.length > 1) {
      this.setState({ open: true });
    }

    this.props.onChange(value);
    this.fetchSuggestions(value);
  }

  onSelect = (_, item) => {
    this.props.onSelect({
      name: item.name,
      crsCode: item.crsCode,
      url: item.url,
    });
    this.setState({ open: false });
  }

  renderInput = (props) => {
    const { name, placeholder, tabIndex } = this.props;

    return (
      <input 
        {...props}
        name={name}
        placeholder={placeholder}
        className={styles.input}
        tabIndex={tabIndex} />
    );
  }

  renderItem(item, isHighlighted) {
    return <div className={`${styles.suggestion} ${isHighlighted && styles.suggestionActive}`} key={uniqueId('autocomplete-item-')}>{item.name}</div>;
  }

  renderMenu(items, value, style) {
    return <div children={items} />;
  }

  render() {
    return (
      <div className={`${styles.stationInput} ${this.props.dark ? styles.dark : ''}`}>
        <Autocomplete
          ref={el => this.inputElem = el}
          getItemValue={item => item.crsCode}
          items={this.state.suggestions}
          onChange={this.onChange}
          renderInput={this.renderInput}
          renderItem={this.renderItem}
          renderMenu={this.renderMenu}
          wrapperProps={{className: styles.inputWrapper}}
          open={this.state.open}
          onSelect={this.onSelect}
          value={this.state.value}
          autoHighlight
          selectOnBlur />
      </div>
    );
  }
}

export default StationInput;
