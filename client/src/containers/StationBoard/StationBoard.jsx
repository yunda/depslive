import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';

import BoardGrid from '../../components/BoardGrid/BoardGrid';

import styles from './styles.css';

const GET_BOARD = gql`
  query RootQuery($from: String!) {  
    board(from: $from) {
      services{
        std
        etd
        destinationName
        platform
        status
      }
    }
  }
`;

export default class StationBoard extends PureComponent {
  static propTypes = {
    crsCode: PropTypes.string,
  }

  render() {
    const vars = { from: this.props.crsCode };
    
    return (
      <Query query={GET_BOARD} variables={vars} notifyOnNetworkStatusChange>
        {({ loading, error, data, refetch, networkStatus }) => {
          const refetching = networkStatus === 4;
          
          if (loading && !refetching) return <div>Loading...</div>;
          if (error) return <div>Error :(</div>;
          
          const { services } = data.board;

          return (
            <div className={styles.board}>
              <button
                onClick={() => {refetch()}}
                className={styles.refreshBtn}
                disabled={refetching}
                title="Refresh">
                {refetching ? 'Refreshing...' : <span className={styles.refreshIcon}>↻</span>}
              </button>
              <BoardGrid services={services} noDestination />
            </div>
          );
        }}
      </Query>
    );
  }
}
