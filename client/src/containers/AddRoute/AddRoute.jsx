import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import isEmpty from 'lodash/isEmpty';

import { addRoute } from '../../store/ducks/page';
import SearchForm from '../SearchForm/SearchForm';

import styles from './styles.css';

const mapStateToProps = state => ({
  newRoute: state.page.newRoute,
  from: state.page.newRoute.from,
  to: state.page.newRoute.to,
});

const mapDispatchToProps = {
  addRoute
};

@connect(mapStateToProps, mapDispatchToProps)
export default class AddRoute extends Component {
  static propsTypes = {
    onRequestClose: PropTypes.func,
    isOpen: PropTypes.bool,
    addRoute: PropTypes.func,
  }
  
  static defaultProps = {
    onRequestClose: () => {}
  }

  submit = () => {
    this.props.addRoute(this.props.newRoute);
    this.props.onRequestClose();
  }

  isSubmitDisabled() {
    return isEmpty(this.props.from) || isEmpty(this.props.to);
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onRequestClose}
        contentLabel="Add Route Modal"
        overlayClassName={styles.modalOverlay}
        className={`${styles.addRoute} ${this.props.isOpen && styles.isOpen}`}>
        <header className={styles.header}>
          <button 
            className={styles.closeButton}
            role="button"
            type="button"
            onClick={this.props.onRequestClose}>Close</button>
          <h1 className={styles.heading}>Choose your route</h1>
        </header>
        <SearchForm className={styles.form} onSubmit={this.submit} hideSubmit dark />
        <div className={styles.actions}>
          <button
            type="submit"
            onClick={this.submit}
            disabled={this.isSubmitDisabled()}
            className={styles.submit}>Add Route</button>
        </div>
      </Modal>
    );
  }
}
