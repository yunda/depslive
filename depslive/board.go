package depslive

import (
	"encoding/xml"
)

type Location struct {
	LocationName string `xml:"locationName"`
	Crs          string `xml:"crs"`
	Tiploc       string `xml:"tiploc"`
}

type StopLocation struct {
	LocationName     string `xml:"locationName" json:"locationName"`
	Tiploc           string `xml:"tiploc" json:"tiploc"`
	Crs              string `xml:"crs" json:"crs"`
	Platform         string `xml:"platform" json:"platform"`
	Sta              string `xml:"sta" json:"sta"`
	Eta              string `xml:"eta" json:"eta"`
	ArrivalType      string `xml:"arrivalType" json:"arrivalType"`
	ArrivalSource    string `xml:"arrivalSource" json:"arrivalSource"`
	Std              string `xml:"std" json:"std"`
	Etd              string `xml:"etd" json:"etd"`
	DepartureType    string `xml:"departureType" json:"departureType"`
	DepartureSource  string `xml:"departureSource" json:"departureSource"`
	PlatformIsHidden bool   `xml:"platformIsHidden" json:"platformIsHidden"`
	IsPass           bool   `xml:"isPass" json:"isPass"`
}

type Service struct {
	Rid                 string         `xml:"rid" json:"rid"`
	Uid                 string         `xml:"uid" json:"uid"`
	Trainid             string         `xml:"trainid" json:"trainid"`
	Rsid                string         `xml:"rsid" json:"rsid"`
	Sdd                 string         `xml:"sdd" json:"sdd"`
	Operator            string         `xml:"operator" json:"operator"`
	OperatorCode        string         `xml:"operatorCode" json:"operatorCode"`
	Std                 string         `xml:"std" json:"std"`
	Etd                 string         `xml:"etd" json:"etd"`
	Sta                 string         `xml:"sta" json:"sta"`
	Eta                 string         `xml:"eta" json:"eta"`
	DepartureType       string         `xml:"departureType" json:"departureType"`
	Platform            string         `xml:"platform" json:"platform"`
	Origin              Location       `xml:"origin>location" json:"origin"`
	Destination         Location       `xml:"destination>location" json:"destination"`
	SubsequentLocations []StopLocation `xml:"subsequentLocations>location" json:"subsequentLocations"`
	PreviousLocations   []StopLocation `xml:"previousLocations>location" json:"previousLocations"`
	IsCancelled         bool           `xml:"isCancelled" json:"isCancelled"`
	CancelReason        string         `xml:"cancelReason" json:"cancelReason"`
	DelayReason         string         `xml:"delayReason" json:"delayReason"`
}

type NrccMessage struct {
	Category     string `xml:"category" json:"category"`
	Severity     string `xml:"severity" json:"severity"`
	XhtmlMessage string `xml:"xhtmlMessage" json:"xhtmlMessage"`
}

type Board struct {
	XMLName            xml.Name      `xml:"Envelope,soap"`
	Crs                string        `xml:"Body>GetDepBoardWithDetailsResponse>GetBoardWithDetailsResult>crs" json:"crs"`
	LocationName       string        `xml:"Body>GetDepBoardWithDetailsResponse>GetBoardWithDetailsResult>locationName" json:"locationName"`
	FilterCrs          string        `xml:"Body>GetDepBoardWithDetailsResponse>GetBoardWithDetailsResult>filtercrs" json:"filterCrs"`
	FilterLocationName string        `xml:"Body>GetDepBoardWithDetailsResponse>GetBoardWithDetailsResult>filterLocationName" json:"filterLocationName"`
	TrainServices      []Service     `xml:"Body>GetDepBoardWithDetailsResponse>GetBoardWithDetailsResult>trainServices>service" json:"services"`
	NrccMessages       []NrccMessage `xml:"Body>GetDepBoardWithDetailsResponse>GetBoardWithDetailsResult>nrccMessages>message" json:"nrccMessages"`
}

type BoardService interface {
	Board(from, to string) (*Board, error)
}
