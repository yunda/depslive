package depslive

type Route struct {
	From Station `json:"from"`
	To   Station `json:"to"`
}

type Page struct {
	Url          string  `json:"url"`
	Email        string  `json:"email"`
	Routes       []Route `json:"routes"`
	Activated    bool    `json:"activated"`
	ActivateHash string  `json:"activateHash"`
	RemoveHash   string  `json:"removeHash"`
	Generic      bool    `json:"generic"`
}

type PageService interface {
	Page(url string) (*Page, error)
	Pages() ([]*Page, error)
	TempPage(url string) (*Page, error)
	CreatePage(p *Page) error
	CreateTempPage(p *Page) error
	DeletePage(url string) error
	TouchGenericPage(from, to string) error
	Exists(url string) bool
	EmailExists(email string) bool
	LinkedToEmail(url, email string) bool
	Incr(url string) error
}
