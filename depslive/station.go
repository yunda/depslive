package depslive

import (
	"encoding/xml"
)

type StationsList struct {
	XMLName  xml.Name  `xml:"StationList"`
	Stations []Station `xml:"Station" json:"stations"`
}

type Address struct {
	Lines    []string `xml:"PostalAddress>A_5LineAddress>Line" json:"lines"`
	PostCode string   `xml:"PostalAddress>A_5LineAddress>PostCode" json:"postCode"`
}

type DayAndTimeAvailability struct {
	DayTypes struct {
		Monday         bool `json:"monday,omitempty"`
		Tuesday        bool `json:"tuesday,omitempty"`
		Wednesday      bool `json:"wednesday,omitempty"`
		Thursday       bool `json:"thursday,omitempty"`
		Friday         bool `json:"friday,omitempty"`
		Saturday       bool `json:"saturday,omitempty"`
		Sunday         bool `json:"sunday,omitempty"`
		Weekend        bool `json:"weekend,omitempty"`
		MondayToSunday bool `json:"mondayToSunday,omitempty"`
		MondayToFriday bool `json:"mondayToFriday,omitempty"`
	} `json:"dayTypes,omitempty"`
	HolidayTypes struct {
		AllBankHoldays bool `json:"allBankHoldays,omitempty"`
	} `json:"holidayTypes,omitempty"`
	OpeningHours struct {
		StartTime       string `xml:"OpenPeriod>StartTime" json:"startTime,omitempty"`
		EndTime         string `xml:"OpenPeriod>EndTime" json:"endTime,omitempty"`
		Unavailable     bool   `json:"unavailable,omitempty"`
		TwentyFourHours bool   `json:"twentyFourHours,omitempty"`
	} `json:"openingHours,omitempty"`
}

type TicketOffice struct {
	Available              string                   `json:"available,omitempty"`
	DayAndTimeAvailability []DayAndTimeAvailability `xml:"Open>DayAndTimeAvailability" json:"dayAndTimeAvailability,omitempty"`
	Annotation             string                   `xml:"Open>Annotation>Note" json:"annotation,omitempty"`
}

type LostProperty struct {
	Available       bool   `xml:"Available" json:"available"`
	TelephoneNumber string `xml:"ContactDetails>PrimaryTelephoneNumber>TelNationalNumber" json:"telephoneNumber"`
	Url             string `xml:"ContactDetails>Url" json:"url"`
}

type Station struct {
	CrsCode            string       `json:"crsCode"`
	Name               string       `json:"name"`
	Longitude          string       `json:"longitude"`
	Latitude           string       `json:"latitude"`
	Url                string       `json:"url"`
	AtmMachine         bool         `xml:"StationFacilities>AtmMachine>Available" json:"atmMachine"`
	Toilets            bool         `xml:"StationFacilities>Toilets>Available" json:"toilets"`
	WiFi               bool         `xml:"StationFacilities>WiFi>Available" json:"wiFi"`
	Telephones         bool         `xml:"StationFacilities>Telephones>Exists" json:"telephones"`
	TelephoneType      string       `xml:"StationFacilities>Telephones>UsageType" json:"telephoneType"`
	WaitingRoom        bool         `xml:"StationFacilities>WaitingRoom>Available" json:"waitingRoom"`
	StationBuffet      bool         `xml:"StationFacilities>StationBuffet>Available" json:"stationBuffet"`
	TicketMachine      bool         `xml:"Fares>TicketMachine>Available" json:"ticketMachine"`
	StepFreeAccess     string       `xml:"Accessibility>StepFreeAccess>Coverage" json:"stepFreeAccess"`
	StepFreeAccessNote string       `xml:"Accessibility>StepFreeAccess>Annotation>Note" json:"stepFreeAccessNote,omitempty"`
	CycleStorage       int32        `xml:"Interchange>CycleStorage>Spaces" json:"cycleStorage"`
	Address            Address      `json:"address"`
	TicketOffice       TicketOffice `xml:"Fares>TicketOffice" json:"ticketOffice"`
	LostProperty       LostProperty `xml:"PassengerServices>LostProperty" json:"lostProperty"`
}

type StationService interface {
	Station(crs string) (*Station, error)
	StationByName(name string) (*Station, error)
	StationsByNames(names []string) ([]*Station, error)
	StationsByCodes(crsCodes []string) ([]*Station, error)
	Names() []string
	Load() error
}
