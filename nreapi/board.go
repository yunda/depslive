package nreapi

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"text/template"
	"time"
)

var templates *template.Template

const soapURL = "https://lite.realtime.nationalrail.co.uk/OpenLDBSVWS/ldbsv10.asmx"

func init() {
	templates = template.Must(template.ParseGlob("nreapi/templates/*"))
}

type BoardService struct {
}

type boardDetailsRequest struct {
	NumRows         int8
	CrsLocationCode string
	FilterCrs       string
	FilterType      string
	TimeWindow      int16
	Time            string
}

func boardDetailsReqXml(from, to string) string {
	var tpl bytes.Buffer
	now := time.Now()
	time := now.Format("2006-01-02T15:04:05")

	req := boardDetailsRequest{
		NumRows:         5,
		CrsLocationCode: from,
		FilterCrs:       to,
		FilterType:      "to",
		TimeWindow:      720,
		Time:            time,
	}

	templates.ExecuteTemplate(&tpl, "getAppDepBoard", &req)

	return tpl.String()
}

func depBoardDetailsReqXml(from, to string) string {
	var tpl bytes.Buffer
	now := time.Now()
	time := now.Format("2006-01-02T15:04:05")

	req := boardDetailsRequest{
		NumRows:         5,
		CrsLocationCode: from,
		FilterCrs:       to,
		FilterType:      "to",
		TimeWindow:      720,
		Time:            time,
	}

	// if len(to) > 0 {
	// 	req.FilterType = "to"
	// }

	fmt.Printf("%+v\n", req)

	templates.ExecuteTemplate(&tpl, "getDepBoard", &req)

	return tpl.String()
}

func RequestDepBoard(from, to string) ([]byte, error) {
	reqXML := depBoardDetailsReqXml(from, to)
	res, err := http.Post(soapURL, "text/xml", strings.NewReader(reqXML))
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return body, nil
}

func RequestBoard(from, to string) ([]byte, error) {
	reqXML := boardDetailsReqXml(from, to)
	res, err := http.Post(soapURL, "text/xml", strings.NewReader(reqXML))
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return body, nil
}

// func (s *BoardService) RequestBoards(fromToPairs []string) ([]*depslive.Board, error) {
// 	boardsChannel := make(chan *depslive.Board, len(fromToPairs))

// 	for _, fromTo := range fromToPairs {
// 		args := strings.Split(fromTo, ":")
// 		boardData, _ := s.RequestBoard(args[0], args[1])
// 		boardsChannel <- boardData
// 	}

// 	close(boardsChannel)

// 	s := make([]*depslive.Board, 0)
// 	for board := range boardsChannel {
// 		s = append(s, board)
// 	}

// 	return s, nil
// }
