package nreapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const dataFeedsUrl = "https://datafeeds.nationalrail.co.uk"
const (
	EMAIL    = "ivan.yunda@gmail.com"
	PASSWORD = "Monday24$"
)

type authResponse struct {
	Token string `json:"token"`
}

func generateToken() (*authResponse, error) {
	data := url.Values{}
	data.Set("username", EMAIL)
	data.Add("password", PASSWORD)

	u, _ := url.ParseRequestURI(dataFeedsUrl)
	u.Path = "/authenticate"
	urlStr := u.String()

	req, _ := http.NewRequest("POST", urlStr, strings.NewReader(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json, text/plain, */*")
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	auth := authResponse{}
	if err := json.Unmarshal(bodyText, &auth); err != nil {
		return nil, err
	}

	return &auth, err
}

func saveStationsResponse(data []byte) {
	err := ioutil.WriteFile("/results/stations.xml", data, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

func RequestStations() ([]byte, error) {
	auth, _ := generateToken()
	req, _ := http.NewRequest("GET", dataFeedsUrl+"/api/staticfeeds/4.0/stations", nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("X-Auth-Token", auth.Token)
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	validBody := NewValidUTF8Reader(resp.Body)
	body, err := ioutil.ReadAll(validBody)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	saveStationsResponse(body)

	return body, nil
}
