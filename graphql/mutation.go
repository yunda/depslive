package graphql

import (
	"depslive/depslive"
	"depslive/mailer"
	"depslive/security"
	"errors"
	"regexp"
	"strings"

	"github.com/badoux/checkmail"
	"github.com/graphql-go/graphql"
	"github.com/mitchellh/mapstructure"
)

var stationInputType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "StationInput",
	Fields: graphql.InputObjectConfigFieldMap{
		"crsCode": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
		"name": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
		"url": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
	},
})

var routeInputType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "RouteInput",
	Fields: graphql.InputObjectConfigFieldMap{
		"from": &graphql.InputObjectFieldConfig{
			Type: stationInputType,
		},
		"to": &graphql.InputObjectFieldConfig{
			Type: stationInputType,
		},
	},
})

var rootMutation = graphql.NewObject(graphql.ObjectConfig{
	Name: "RootMutation",
	Fields: graphql.Fields{
		"createPage": &graphql.Field{
			Type: pageType, // the return type for this field
			Args: graphql.FieldConfigArgument{
				"url": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"routes": &graphql.ArgumentConfig{
					Type: graphql.NewList(routeInputType),
				},
				"override": &graphql.ArgumentConfig{
					Type:         graphql.Boolean,
					DefaultValue: false,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				email, _ := params.Args["email"].(string)

				err := checkmail.ValidateFormat(email)
				if err != nil {
					return nil, errors.New("email:email format is incorrect")
				}

				err = checkmail.ValidateHost(email)
				if err != nil {
					return nil, errors.New("email:email account does not exist")
				}
				url, _ := params.Args["url"].(string)

				urlRegEx := regexp.MustCompile(`^[0-9a-zA-Z-]+$`)

				if !urlRegEx.MatchString(url) {
					return nil, errors.New("url:URL is invalid")
				}

				if pageService.Exists(url) && !pageService.LinkedToEmail(url, email) {
					return nil, errors.New("url:url is already taken")
				}

				override, _ := params.Args["override"].(bool)

				if pageService.EmailExists(email) && !pageService.LinkedToEmail(url, email) && !override {
					return nil, errors.New("exists:another page linked to this email already exists")
				}

				rr, _ := params.Args["routes"].([]interface{})

				routes := []depslive.Route{}

				for _, r := range rr {
					route := depslive.Route{}
					mapstructure.Decode(r, &route)
					routes = append(routes, route)
				}

				activateHash, activateToken := security.GenerateToken("activate" + url + email)
				removeHash, removeToken := security.GenerateToken("remove" + url + email)
				page := depslive.Page{
					Url:          strings.ToLower(url),
					Email:        email,
					Routes:       routes,
					ActivateHash: activateHash,
					RemoveHash:   removeHash,
					Generic:      false,
				}

				pageService.CreateTempPage(&page)
				mailer.SendActivationEmail(page.Email, page.Url, activateToken, removeToken)

				return &page, nil
			},
		},
	},
})
