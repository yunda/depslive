package graphql

import (
	"depslive/depslive"
	"fmt"
	"strings"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/schollz/closestmatch"
	"github.com/thoas/go-funk"
)

var cm *closestmatch.ClosestMatch
var stationNames []string

func init() {
	stationNames = stationService.Names()
	bagSizes := []int{3}
	cm = closestmatch.New(stationNames, bagSizes)
}

var locationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Location",
	Fields: graphql.Fields{
		"locationName": &graphql.Field{
			Type: graphql.String,
		},
		"crs": &graphql.Field{
			Type: graphql.String,
		},
		"std": &graphql.Field{
			Type: graphql.String,
		},
		"etd": &graphql.Field{
			Type: graphql.String,
		},
		"sta": &graphql.Field{
			Type: graphql.String,
		},
		"eta": &graphql.Field{
			Type: graphql.String,
		},
		"isPass": &graphql.Field{
			Type: graphql.Boolean,
		},
		"platformIsHidden": &graphql.Field{
			Type: graphql.Boolean,
		},
		"platform": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var serviceType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Service",
	Fields: graphql.Fields{
		"rid": &graphql.Field{
			Type: graphql.String,
		},
		"operator": &graphql.Field{
			Type: graphql.String,
		},
		"operatorCode": &graphql.Field{
			Type: graphql.String,
		},
		"std": &graphql.Field{
			Type: graphql.String,
		},
		"etd": &graphql.Field{
			Type: graphql.String,
		},
		"sta": &graphql.Field{
			Type: graphql.String,
		},
		"eta": &graphql.Field{
			Type: graphql.String,
		},
		"platform": &graphql.Field{
			Type: graphql.String,
		},
		"origin": &graphql.Field{
			Type: graphql.String,
		},
		"isCancelled": &graphql.Field{
			Type: graphql.Boolean,
		},
		"cancelReason": &graphql.Field{
			Type: graphql.String,
		},
		"delayReason": &graphql.Field{
			Type: graphql.String,
		},
		"destinationName": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				service := params.Source.(depslive.Service)

				return funk.Get(service, "Destination.LocationName"), nil
			},
		},
		"originName": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				service := params.Source.(depslive.Service)

				return funk.Get(service, "Origin.LocationName"), nil
			},
		},
		"subsequentLocations": &graphql.Field{
			Type: graphql.NewList(locationType),
			Args: graphql.FieldConfigArgument{
				"isPass": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				isPass := params.Args["isPass"].(bool)
				service := params.Source.(depslive.Service)
				locations := service.SubsequentLocations

				return funk.Filter(locations, func(loc depslive.StopLocation) bool {
					return loc.IsPass == isPass
				}), nil
			},
		},
		"previousLocations": &graphql.Field{
			Type: graphql.NewList(locationType),
			Args: graphql.FieldConfigArgument{
				"isPass": &graphql.ArgumentConfig{
					Type: graphql.Boolean,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				isPass := params.Args["isPass"].(bool)
				service := params.Source.(depslive.Service)
				locations := service.SubsequentLocations

				return funk.Filter(locations, func(loc depslive.StopLocation) bool {
					return loc.IsPass == isPass
				}), nil
			},
		},
		"stopsNumber": &graphql.Field{
			Type: graphql.Int,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				filterSrc, ok := params.Info.VariableValues["to"]

				if !ok {
					return nil, nil
				}

				service := params.Source.(depslive.Service)
				locations := service.SubsequentLocations
				stops := funk.Map(funk.Filter(locations, func(loc depslive.StopLocation) bool {
					return loc.IsPass == false && len(loc.Crs) > 0
				}), func(loc depslive.StopLocation) string {
					return loc.Crs
				}).([]string)
				indexOfDest := funk.IndexOf(stops, filterSrc)

				return len(stops[0:indexOfDest]), nil
			},
		},
		"arrival": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				filterSrc, ok := params.Info.VariableValues["to"]

				if !ok {
					return nil, nil
				}

				service := params.Source.(depslive.Service)
				locations := service.SubsequentLocations
				destStop := funk.Find(locations, func(loc depslive.StopLocation) bool {
					return loc.Crs == filterSrc
				}).(depslive.StopLocation)

				return destStop.Eta, nil
			},
		},
		"status": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				service := params.Source.(depslive.Service)

				if service.IsCancelled {
					return "Canceled", nil
				}

				if len(service.Etd) == 0 {
					return "On time", nil
				}

				layout := "2006-01-02T15:04:05"

				stdTime, err := time.Parse(layout, service.Std)

				if err != nil {
					panic(err)
				}

				etdTime, err := time.Parse(layout, service.Etd)

				if err != nil {
					panic(err)
				}

				if etdTime.After(stdTime) {
					return "Delayed", nil
				}

				return "On time", nil
			},
		},
		"travelTime": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				filterSrc, ok := params.Info.VariableValues["to"]

				if !ok {
					return nil, nil
				}

				service := params.Source.(depslive.Service)
				locations := service.SubsequentLocations
				destStop := funk.Find(locations, func(loc depslive.StopLocation) bool {
					return loc.Crs == filterSrc
				}).(depslive.StopLocation)
				layout := "2006-01-02T15:04:05"
				arrivalTime, err := time.Parse(layout, destStop.Eta)

				if err != nil {
					panic(err)
				}

				stdTime, err := time.Parse(layout, service.Std)

				if err != nil {
					panic(err)
				}

				diff := arrivalTime.Sub(stdTime)

				return shortDur(diff), nil
			},
		},
	},
})

var nrccMessageType = graphql.NewObject(graphql.ObjectConfig{
	Name: "NrccMessage",
	Fields: graphql.Fields{
		"category": &graphql.Field{
			Type: graphql.String,
		},
		"severity": &graphql.Field{
			Type: graphql.String,
		},
		"xhtmlMessage": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var boardType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Board",
	Fields: graphql.Fields{
		"crs": &graphql.Field{
			Type: graphql.String,
		},
		"locationName": &graphql.Field{
			Type: graphql.String,
		},
		"filterCrs": &graphql.Field{
			Type: graphql.String,
		},
		"filterLocationName": &graphql.Field{
			Type: graphql.String,
		},
		"services": &graphql.Field{
			Type: graphql.NewList(serviceType),
		},
		"nrccMessages": &graphql.Field{
			Type: graphql.NewList(nrccMessageType),
		},
	},
})

var addressType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Address",
	Fields: graphql.Fields{
		"lines": &graphql.Field{
			Type: graphql.NewList(graphql.String),
		},
		"postCode": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var dayTypesType = graphql.NewObject(graphql.ObjectConfig{
	Name: "DayTypes",
	Fields: graphql.Fields{
		"monday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"tuesday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"wednesday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"thursday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"friday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"saturday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"sunday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"weekend": &graphql.Field{
			Type: graphql.Boolean,
		},
		"mondayToSunday": &graphql.Field{
			Type: graphql.Boolean,
		},
		"mondayToFriday": &graphql.Field{
			Type: graphql.Boolean,
		},
	},
})

var openingHoursType = graphql.NewObject(graphql.ObjectConfig{
	Name: "OpeningHours",
	Fields: graphql.Fields{
		"startTime": &graphql.Field{
			Type: graphql.String,
		},
		"endTime": &graphql.Field{
			Type: graphql.String,
		},
		"unavailable": &graphql.Field{
			Type: graphql.Boolean,
		},
		"twentyFourHours": &graphql.Field{
			Type: graphql.Boolean,
		},
	},
})

var holidayTypesType = graphql.NewObject(graphql.ObjectConfig{
	Name: "HolidayTypes",
	Fields: graphql.Fields{
		"allBankHoldays": &graphql.Field{
			Type: graphql.Boolean,
		},
	},
})

var dayAndTimeAvailabilityType = graphql.NewObject(graphql.ObjectConfig{
	Name: "DayAndTimeAvailability",
	Fields: graphql.Fields{
		"dayTypes": &graphql.Field{
			Type: dayTypesType,
		},
		"dayType": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				dayTypes := params.Source.(depslive.DayAndTimeAvailability).DayTypes
				days := []string{
					"Monday",
					"Tuesday",
					"Wednesday",
					"Thursday",
					"Friday",
					"Saturday",
					"Sunday",
				}
				seq := false
				result := []string{}

				if dayTypes.MondayToFriday {
					return "Mon – Fri", nil
				}

				if dayTypes.MondayToSunday {
					return "Mon – Sun", nil
				}

				if dayTypes.Weekend {
					return "Weekend", nil
				}

				for i, d := range days {
					if funk.Get(dayTypes, d).(bool) {
						if len(result) != 0 {
							last := result[len(result)-1]
							lastI := funk.IndexOf(days, last)
							if lastI == (i - 1) {
								seq = true
							} else {
								seq = false
							}
						}

						result = append(result, d)
					}
				}

				if seq {
					first := result[0]
					last := result[len(result)-1]

					return first[:3] + " – " + last[:3], nil
				}

				result = funk.Map(result, func(d string) string {
					return d[:3]
				}).([]string)

				return strings.Join(result[:], ", "), nil
			},
		},
		"openingHours": &graphql.Field{
			Type: openingHoursType,
		},
		"holidayTypes": &graphql.Field{
			Type: holidayTypesType,
		},
	},
})

var ticketOfficeType = graphql.NewObject(graphql.ObjectConfig{
	Name: "TicketOffice",
	Fields: graphql.Fields{
		"available": &graphql.Field{
			Type: graphql.String,
		},
		"annotation": &graphql.Field{
			Type: graphql.String,
		},
		"dayAndTimeAvailability": &graphql.Field{
			Type: graphql.NewList(dayAndTimeAvailabilityType),
		},
	},
})

var stationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Station",
	Fields: graphql.Fields{
		"crsCode": &graphql.Field{
			Type: graphql.String,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"longitude": &graphql.Field{
			Type: graphql.String,
		},
		"latitude": &graphql.Field{
			Type: graphql.String,
		},
		"url": &graphql.Field{
			Type: graphql.String,
		},
		"atmMachine": &graphql.Field{
			Type: graphql.Boolean,
		},
		"toilets": &graphql.Field{
			Type: graphql.Boolean,
		},
		"wiFi": &graphql.Field{
			Type: graphql.Boolean,
		},
		"telephones": &graphql.Field{
			Type: graphql.Boolean,
		},
		"telephoneType": &graphql.Field{
			Type: graphql.String,
		},
		"waitingRoom": &graphql.Field{
			Type: graphql.Boolean,
		},
		"stationBuffet": &graphql.Field{
			Type: graphql.Boolean,
		},
		"ticketMachine": &graphql.Field{
			Type: graphql.Boolean,
		},
		"stepFreeAccess": &graphql.Field{
			Type: graphql.Boolean,
		},
		"stepFreeAccessNote": &graphql.Field{
			Type: graphql.String,
		},
		"cycleStorage": &graphql.Field{
			Type: graphql.Boolean,
		},
		"address": &graphql.Field{
			Type: addressType,
		},
		"ticketOffice": &graphql.Field{
			Type: ticketOfficeType,
		},
	},
})

var routeType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Route",
	Fields: graphql.Fields{
		"from": &graphql.Field{
			Type: stationType,
		},
		"to": &graphql.Field{
			Type: stationType,
		},
	},
})

var pageType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Page",
	Fields: graphql.Fields{
		"url": &graphql.Field{
			Type: graphql.String,
		},
		"email": &graphql.Field{
			Type: graphql.String,
		},
		"routes": &graphql.Field{
			Type: graphql.NewList(routeType),
		},
	},
})

var rootQuery = graphql.NewObject(graphql.ObjectConfig{
	Name: "RootQuery",
	Fields: graphql.Fields{
		"page": &graphql.Field{
			Type: pageType,
		},
		"board": &graphql.Field{
			Type: boardType,
			Args: graphql.FieldConfigArgument{
				"from": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"to": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				from, fromIsOk := params.Args["from"].(string)
				to, toIsOk := params.Args["to"].(string)

				if fromIsOk {
					if toIsOk && to != "" {
						fmt.Println("ok")
						go pageService.TouchGenericPage(from, to)
					}
					res, err := boardService.Board(from, to)
					return res, err
				}

				return nil, nil
			},
		},
		"stations": &graphql.Field{
			Type: graphql.NewList(stationType),
			Args: graphql.FieldConfigArgument{
				"query": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"crsCodes": &graphql.ArgumentConfig{
					Type: graphql.NewList(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				crsCodes, ok := params.Args["crsCodes"].([]interface{})
				fmt.Println(crsCodes)
				fmt.Println(ok)
				if ok {
					codes := make([]string, len(crsCodes))
					for i, v := range crsCodes {
						codes[i] = v.(string)
					}
					return stationService.StationsByCodes(codes)
				}

				if params.Args["query"] == nil {
					return stationService.Stations()
				}

				q := params.Args["query"].(string)
				var names []string
				max := 3

				containMathes := funk.Filter(stationNames, func(item string) bool {
					return strings.Contains(strings.ToLower(item), strings.ToLower(q))
				}).([]string)
				foundN := len(containMathes)
				var fuzzyMatches []string

				if foundN == max {
					names = containMathes
				} else if foundN > max {
					names = containMathes[:max]
				} else {
					fuzzyMatches = cm.ClosestN(q, max)
					names = funk.Uniq(append(containMathes, fuzzyMatches...)).([]string)
				}

				if len(names) == 0 {
					return []interface{}{}, nil
				}

				return stationService.StationsByNames(names)
			},
		},
		"station": &graphql.Field{
			Type: stationType,
			Args: graphql.FieldConfigArgument{
				"url": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				url := params.Args["url"].(string)

				if len(url) != 0 {
					return stationService.StationByUrl(url)
				}

				return nil, nil
			},
		},
	},
})
