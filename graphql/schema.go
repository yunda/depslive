package graphql

import (
	"depslive/redis"

	"github.com/graphql-go/graphql"
)

var pageService redis.PageService
var boardService redis.BoardService
var stationService redis.StationService

var Schema, _ = graphql.NewSchema(graphql.SchemaConfig{
	Query:    rootQuery,
	Mutation: rootMutation,
})
