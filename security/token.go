package security

import (
	"crypto/md5"
	"encoding/hex"
	"log"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

func GenerateToken(key string) (string, string) {
	hash, err := bcrypt.GenerateFromPassword([]byte(key), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	hasher := md5.New()
	hasher.Write(hash)

	return string(hash), hex.EncodeToString(hasher.Sum(nil))
}

func GenerateTokenFromHash(hash string) string {
	hasher := md5.New()
	hasher.Write([]byte(hash))

	return hex.EncodeToString(hasher.Sum(nil))
}

func CompareToken(hash, token string) bool {
	hashToken := GenerateTokenFromHash(hash)

	return hashToken == token
}

func GravatarMD5(url string) (string, string) {
	h := md5.New()
	h.Write([]byte(strings.ToLower(url)))

	return string(h.Sum(nil)), hex.EncodeToString(h.Sum(nil))
}
