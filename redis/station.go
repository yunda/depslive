package redis

import (
	"depslive/depslive"
	"depslive/nreapi"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"regexp"
	"strings"

	"github.com/go-redis/redis"
	funk "github.com/thoas/go-funk"
)

type StationService struct {
}

func saveRawStationsResponse(body []byte) {
	// TODO add expiration
	err := client.Set("stationsResponse", body, 0).Err()
	if err != nil {
		panic(err)
	}

	fmt.Println("stations response saved")
}

func rawStationsResponse() []byte {
	val, err := client.Get("stationsResponse").Result()

	if err == redis.Nil {
		return nil
	} else if err != nil {
		panic(err)
		return nil
	}

	return []byte(val)
}

func serializeIterator(k string, v depslive.Station) (string, interface{}) {
	s, err := json.Marshal(v)

	if err != nil {
		panic(err)
	}

	return k, s
}

func saveStations(stations []depslive.Station) {
	stationsByCrs := funk.ToMap(stations, "CrsCode")
	stationsByName := funk.ToMap(stations, "Name")
	stationsByUrl := funk.ToMap(stations, "Url")

	stationsByCrsSerialized := funk.Map(stationsByCrs, serializeIterator).(map[string]interface{})
	stationsByNameSerialized := funk.Map(stationsByName, serializeIterator).(map[string]interface{})
	stationsByUrlSerialized := funk.Map(stationsByUrl, serializeIterator).(map[string]interface{})

	err := client.HMSet("stationsByCrs", stationsByCrsSerialized).Err()

	if err != nil {
		panic(err)
	}

	err = client.HMSet("stationsByName", stationsByNameSerialized).Err()

	if err != nil {
		panic(err)
	}

	err = client.HMSet("stationsByUrl", stationsByUrlSerialized).Err()

	if err != nil {
		panic(err)
	}
}

func createUrl(name string) string {
	andRe := regexp.MustCompile(`(&)`)
	spaceRe := regexp.MustCompile(`\s`)
	notUrlCharRe := regexp.MustCompile(`[^0-9a-zA-Z-]`)

	noAnd := andRe.ReplaceAllString(name, "and")
	noSpace := spaceRe.ReplaceAllString(noAnd, "-")
	noOthers := notUrlCharRe.ReplaceAllString(noSpace, "")

	return strings.ToLower(noOthers)
}

func (s *StationService) Load() {
	res := rawStationsResponse()

	if res == nil {
		res, _ = nreapi.RequestStations()
		saveRawStationsResponse(res)
	}

	stations := depslive.StationsList{}
	if err := xml.Unmarshal(res, &stations); err != nil {
		panic(err)
	}

	stationsWithUrl := funk.Map(stations.Stations, func(s depslive.Station) depslive.Station {
		s.Url = createUrl(s.Name)
		return s
	}).([]depslive.Station)

	saveStations(stationsWithUrl)
}

func (s *StationService) Names() []string {
	val, err := client.HKeys("stationsByName").Result()

	if err != nil {
		panic(err)
	}

	return val
}

func (s *StationService) Stations() ([]*depslive.Station, error) {
	stationsMap, err := client.HGetAll("stationsByName").Result()

	fmt.Println("HGetAll")
	if err != nil {
		panic(err)
	}

	fmt.Println("No error")

	vals := funk.Values(stationsMap)

	stations := funk.Map(vals, func(item string) *depslive.Station {
		s := depslive.Station{}
		err := json.Unmarshal([]byte(item), &s)

		if err != nil {
			panic(err)
		}

		return &s
	})

	return stations.([]*depslive.Station), nil
}

func (s *StationService) StationsByNames(names []string) ([]*depslive.Station, error) {
	vals, err := client.HMGet("stationsByName", names...).Result()

	if err != nil {
		return nil, err
	}

	stations := funk.Map(vals, func(item interface{}) *depslive.Station {
		s := depslive.Station{}
		err := json.Unmarshal([]byte(item.(string)), &s)

		if err != nil {
			panic(err)
		}

		return &s
	})

	return stations.([]*depslive.Station), nil
}

func (s *StationService) StationsByCodes(crsCodes []string) ([]*depslive.Station, error) {
	vals, err := client.HMGet("stationsByCrs", crsCodes...).Result()

	if err != nil {
		return nil, err
	}

	stations := funk.Map(vals, func(item interface{}) *depslive.Station {
		s := depslive.Station{}
		err := json.Unmarshal([]byte(item.(string)), &s)

		if err != nil {
			panic(err)
		}

		return &s
	}).([]*depslive.Station)

	return stations, nil
}

func (s *StationService) StationByCode(crsCode string) (*depslive.Station, error) {
	val, err := client.HMGet("stationsByCrs", crsCode).Result()

	if err != nil {
		return nil, err
	}

	station := depslive.Station{}
	err = json.Unmarshal([]byte(val[0].(string)), &station)

	if err != nil {
		panic(err)
	}

	return &station, nil
}

func (s *StationService) StationByUrl(url string) (*depslive.Station, error) {
	val, err := client.HMGet("stationsByUrl", url).Result()

	fmt.Println(val)

	if err != nil {
		return nil, err
	}

	station := depslive.Station{}
	err = json.Unmarshal([]byte(val[0].(string)), &station)

	if err != nil {
		panic(err)
	}

	return &station, nil
}
