package redis

import (
	"depslive/depslive"
	"depslive/nreapi"
	"encoding/xml"
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

const (
	boardKey string = "boards.%s:%s"
)

type BoardService struct {
}

func getCachedBoard(from, to string) []byte {
	val, err := client.Get(fmt.Sprintf(boardKey, from, to)).Result()

	if err == redis.Nil {
		return nil
	} else if err != nil {
		return nil
	}
	fmt.Println("board served from cache")

	return []byte(val)
}

func setBoardCache(from string, to string, body []byte) {
	err := client.Set(fmt.Sprintf(boardKey, from, to), body, time.Second*5).Err()
	if err != nil {
		panic(err)
	}

	fmt.Println("board cache saved")
}

func (s *BoardService) Board(from, to string) (*depslive.Board, error) {
	res := getCachedBoard(from, to)

	if res == nil {
		res, _ = nreapi.RequestDepBoard(from, to)
		// fmt.Println(err)
		// fmt.Println(string(res))
		setBoardCache(from, to, res)
	}

	data := depslive.Board{}

	if err := xml.Unmarshal(res, &data); err != nil {
		return nil, err
	}

	// fmt.Printf("%+v\n", data)

	return &data, nil
}
