package redis

import (
	"depslive/depslive"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis"
)

const (
	tempPageKey        string = "tempPage."
	tempPageByEmailKey string = "tempPageByEmail."
	pageKey            string = "page."
	pageByEmailKey     string = "pageByEmail."
	pageCounterKey     string = "pageCounter."
)

type PageService struct {
}

var stationService StationService

func (s *PageService) marshal(page *depslive.Page) ([]byte, error) {
	return json.Marshal(page)
}

func (s *PageService) unmarshal(data []byte) (*depslive.Page, error) {
	var page depslive.Page
	if err := json.Unmarshal(data, &page); err != nil {
		return nil, err
	}

	return &page, nil
}

func (s *PageService) PageOrTempPage(url string) (*depslive.Page, error) {
	page, _ := s.Page(url)

	if page == nil {
		return s.TempPage(url)
	}

	return page, nil
}

func (s *PageService) cleanUpOldPage(email string) {
	oldUrl, err := client.Get(pageByEmailKey + email).Result()

	if err == nil {
		client.Del(pageKey+oldUrl, pageCounterKey+oldUrl)
	}

	oldTempUrl, err := client.Get(tempPageByEmailKey + email).Result()

	if err == nil {
		client.Del(tempPageKey + oldTempUrl)
	}
}

func (s *PageService) pageUrlByEmail(email string) string {
	pageUrl, err := client.Get(pageByEmailKey + email).Result()

	if err == redis.Nil {
		pageUrl = client.Get(tempPageByEmailKey + email).Val()
	}

	return pageUrl
}

func (s *PageService) CreateTempPage(page *depslive.Page) error {
	pageJSON, err := s.marshal(page)
	if err != nil {
		return err
	}
	url := page.Url
	email := page.Email

	s.cleanUpOldPage(email)

	err = client.Set(tempPageKey+url, pageJSON, time.Hour*2).Err()
	if err != nil {
		return err
	}

	err = client.Set(tempPageByEmailKey+email, url, time.Hour*2).Err()
	if err != nil {
		return err
	}

	return nil
}

func (s *PageService) TempPage(url string) (*depslive.Page, error) {
	val, err := client.Get(tempPageKey + url).Result()

	if err != nil {
		return nil, err
	}

	if val == "" {
		return nil, nil
	}

	return s.unmarshal([]byte(val))
}

func (s *PageService) CreatePage(page *depslive.Page) error {
	pageJSON, err := s.marshal(page)
	if err != nil {
		return err
	}

	url := page.Url
	email := page.Email

	if !page.Generic {
		client.Del(tempPageKey+url, tempPageByEmailKey+email)
		s.cleanUpOldPage(email)
	}

	err = client.Set(pageKey+url, pageJSON, 0).Err()
	if err != nil {
		return err
	}

	err = client.Set(pageByEmailKey+email, url, 0).Err()
	if err != nil {
		return err
	}

	err = client.Set(pageCounterKey+url, 1, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (s *PageService) Page(url string) (*depslive.Page, error) {
	val, err := client.Get(pageKey + url).Result()

	if err != nil {
		return nil, err
	}

	if val == "" {
		return nil, nil
	}

	return s.unmarshal([]byte(val))
}

func (s *PageService) TouchGenericPage(from, to string) error {
	stations, err := stationService.StationsByCodes([]string{from, to})

	if err != nil {
		log.Fatal(err)
		return err
	}

	fromS := stations[0]
	toS := stations[1]
	pageUrl := fmt.Sprintf("trains/from/%s/to/%s", fromS.Url, toS.Url)

	if s.Exists(pageUrl) {
		return s.Incr(pageUrl)
	}

	routes := []depslive.Route{
		depslive.Route{
			From: *fromS,
			To:   *toS,
		},
	}
	page := depslive.Page{
		Url:       pageUrl,
		Email:     "admin@departures.live",
		Routes:    routes,
		Activated: true,
		Generic:   true,
	}

	return s.CreatePage(&page)
}

func (s *PageService) DeletePage(url string, email string) {
	urlKey := pageKey + url
	emailKey := pageByEmailKey + email
	counterKey := pageCounterKey + url
	tempUrlKey := tempPageKey + url
	tempEmailKey := tempPageByEmailKey + email

	client.Del(urlKey, emailKey, counterKey, tempUrlKey, tempEmailKey)
}

func (s *PageService) Exists(url string) bool {
	pageExists := client.Exists(pageKey + url).Val()
	tempPageExists := client.Exists(tempPageKey + url).Val()

	return pageExists == 1 || tempPageExists == 1
}

func (s *PageService) EmailExists(email string) bool {
	emailExists := client.Exists(pageByEmailKey + email).Val()
	tempEmailExists := client.Exists(tempPageByEmailKey + email).Val()

	return emailExists == 1 || tempEmailExists == 1
}

func (s *PageService) LinkedToEmail(url, email string) bool {
	pageUrl := s.pageUrlByEmail(email)

	return pageUrl == url
}

func (s *PageService) Incr(url string) error {
	return client.Incr(pageCounterKey + url).Err()
}
