package webserver

import (
	"depslive/depslive"
	"depslive/graphql"
	"depslive/redis"
	"depslive/security"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/graphql-go/handler"
	"github.com/rs/cors"
)

var pageService redis.PageService

type AppState struct {
	Url       string           `json:"url"`
	Routes    []depslive.Route `json:"routes"`
	Activated bool             `json:"activated"`
}

func createGraphQLHandler() gin.HandlerFunc {
	// Creates a GraphQL-go HTTP handler with the defined schema
	// GraphQL server
	h := handler.New(&handler.Config{
		Schema:   &graphql.Schema,
		Pretty:   true,
		GraphiQL: true,
	})

	corsHandler := cors.Default().Handler(h)

	return func(c *gin.Context) {
		corsHandler.ServeHTTP(c.Writer, c.Request)
	}
}

func defaultHandler(c *gin.Context) {
	url := strings.TrimSuffix(string(c.Request.URL.Path[1:]), "/")
	pageExists := pageService.Exists(url)
	state := "{}"

	fmt.Println(url, pageExists)

	if pageExists {
		page, err := pageService.PageOrTempPage(url)

		fmt.Printf("%+v\n", page)

		if err == nil {
			appState := AppState{
				Activated: page.Activated,
				Routes:    page.Routes,
			}

			stateJSON, _ := json.Marshal(appState)
			state = string(stateJSON)
		}
	}

	c.HTML(http.StatusOK, "index.html", gin.H{
		"Title":          "Trains dashboard",
		"PreloadedState": state,
	})
}

func activatePageHandler(c *gin.Context) {
	url := c.Param("url")
	token := c.Query("token")
	tempPage, err := pageService.TempPage(url)

	if err != nil || token == "" {
		c.Redirect(http.StatusSeeOther, "/invalid-token")
		return
	}

	tokenMatches := security.CompareToken(tempPage.ActivateHash, token)

	if !tokenMatches {
		c.Redirect(http.StatusSeeOther, "/invalid-token")
		return
	}

	tempPage.Activated = true

	pageService.CreatePage(tempPage)
	c.Redirect(http.StatusSeeOther, "/activation-success/"+tempPage.Url)
}

func removePageHandler(c *gin.Context) {
	url := c.Param("url")
	token := c.Query("token")
	page, err := pageService.PageOrTempPage(url)

	if err != nil || token == "" {
		c.Redirect(http.StatusSeeOther, "/invalid-token")
		return
	}

	tokenMatches := security.CompareToken(page.RemoveHash, token)

	if !tokenMatches {
		c.Redirect(http.StatusSeeOther, "/invalid-token")
		return
	}

	pageService.DeletePage(page.Url, page.Email)

	c.Redirect(http.StatusSeeOther, "/removal-success/"+page.Url)
}

func RegisterRoutes() *gin.Engine {
	// Creates a gin router with default middleware:
	// logger and recovery (crash-free) middleware
	router := gin.Default()

	router.LoadHTMLFiles("./client/build/index.html")

	router.Any("/graphql", createGraphQLHandler())

	router.Static("/assets", "./client/build/")

	router.GET("/activate/:url", activatePageHandler)
	router.GET("/remove/:url", removePageHandler)

	router.GET("/", defaultHandler)

	router.NoRoute(defaultHandler)

	return router
}
